const { response } = require('express')
const { model } = require('mongoose')
const task = require('../Models/task')
const Task = require('../Models/task')


//Controllers and functions


module.exports.getAll = (req, res)=>{

    Task.find({}).then(result => {
        //to capture the result of the find method
        return res.send(result)
    })
    //.catch method captures the error when the find method is executed
    .catch(error => {
        return res.send(error)
    })
}

module.exports.createTask = (req, res) => {
    const input = req.body;

    Task.findOne({name: input.name})
    .then(result =>{
        if(result !== null){
            return res.send("The task is already existing!")
        }else{
            let newTask = new Task({
                name: input.name
            });

            newTask.save().then(save => {
                return res.send("The task is successfully added!")
            }).catch(error=>{
                return res.send(error)
            })
        }
    })
    .catch(error =>{
        return res.send(error)
    })
};

//Controller that will delete the document that contains

module.exports.deleteTask = (req,res) => {
    let idTobBeDeleted = req.params.id;
    

    //findByIdAndRemove - to find the document that contains the id and then delete the document
    Task.findByIdAndRemove(idTobBeDeleted)
    .then(result => {
        return res.send(result)
    })
    .catch(error => {
        return res.send(error)
    })
};

//Controller that will get the documents with id params on it
module.exports.getTask = (req,res) => {
    let idToberetrieve = req.params.id;

    Task.findById(idToberetrieve)
    .then(result =>{
        return res.send(result)
    })
    .catch(error =>{
        return res.send(error)
    })
}

//Controller that will update the status of the task retrieved

module.exports.updateTask = (req,res) =>{
    let idTobeUpdate = req.params.id;
    let input = req.body

   Task.findByIdAndUpdate(idTobeUpdate, {status:input.status},{new:true}, function(error, result){
    if(error){
        res.send(error)
    }else{
        res.send(result)
    }
   })
 
}

// 


