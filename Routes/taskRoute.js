const { application } = require('express');
const express = require('express');
const router = express.Router();

const taskController = require('../Controllers/taskControllers')



//routes

router.get('/tasks', taskController.getAll)
router.post('/addTask', taskController.createTask)

//route for delete task

router.delete('/deleteTasks/:id',taskController.deleteTask)

//route for retrieve task with id
router.get('/:id', taskController.getTask)

//route for updating the status of task
router.put('/:id/complete', taskController.updateTask)




module.exports = router;